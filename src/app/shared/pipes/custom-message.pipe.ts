import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customMessage'
})
export class CustomMessagePipe implements PipeTransform {

  transform(value: string): string {
    return `Bienvenido ${value}`;
  }

}
