import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomMessagePipe } from './custom-message.pipe';



@NgModule({
  declarations: [
    CustomMessagePipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CustomMessagePipe
  ]
})
export class PipesModule { }
