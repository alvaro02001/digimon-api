import { IDigimon } from "../models/digimons"; 
export const mapServiceToDigimon = (data: any): IDigimon =>{   
            return{
                name:data?.name,//Cogemos datos
                images:data?.images,//Cogemos datos
                levels:data?.levels,//Cogemos datos, solo el 0
                attributes:data?.attributes,//Cogemos datos, solo el 0
                fields:data?.fields,
                descriptions:data?.descriptions,//Cogemos datos, la uno (1).
                priorEvolutions:data?.priorEvolutions,
                nextEvolutions:data?.nextEvolutions
            }
        }; //Hay que meter aquí lo que hay en el digimon.ts de models dentro del return{}