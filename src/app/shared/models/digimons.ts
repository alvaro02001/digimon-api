export interface IDigimon {
    name?: string;
    images?: any[];
    levels?: any[];
    attributes?: any[];
    fields?: any[];
    descriptions?: any[];
    priorEvolutions?: any[];
    nextEvolutions?: any[];
}
