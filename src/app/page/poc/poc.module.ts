import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PocComponent } from './poc.component';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { PocRoutingModule } from './poc-routing.module';



@NgModule({
  declarations: [
    PocComponent
  ],
  imports: [
    CommonModule,
    PocRoutingModule,
    PipesModule
  ]
})
export class PocModule { }
