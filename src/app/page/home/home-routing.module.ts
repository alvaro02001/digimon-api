import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PocComponent } from '../poc/poc.component';
import { HomeComponent } from './home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },

{
  path:'poc',
  component:PocComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }

