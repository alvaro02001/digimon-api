import { Component } from '@angular/core';
import { DapiService } from './services/business/dapi.service';
import { IMAGE_CONST } from './shared/constants/images.constats';


interface Icard{
    name: string,
    body: any,
    actions: any
}
@Component({
 selector: 'app-root',
 templateUrl: './app.component.html',
 styleUrls: ['./app.component.scss']
})



export class AppComponent {
 title = 'azujilFront';
 cardConfig: Icard={
    name: 'card prueba',
    actions: ['click', 'limpiar'],
    body: 'Hola soy una prueba'
 };

logoUrl: string = '';

 //Lo que esta dentro del constructor es lo que se ejecuta antes al cargar la página.
 constructor(
    private DapiService: DapiService,
 ){
    const [{route = ''} = {}] = IMAGE_CONST.filter(a => a.name === 'main-logo')
    this.logoUrl = route;
    
 }
 searchByFilter(event:any){
   const {name="Skull Greymon",level="3", description="a", atribute="vaccine"}=event;
  
   this.DapiService.getDigimonList(name, level, description, atribute ).subscribe(data => {
      console.log(data);
      
   })
   // name, level, description, atribute 
 }
}