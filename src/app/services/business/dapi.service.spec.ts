import { TestBed } from '@angular/core/testing';

import { DapiService } from './dapi.service';

describe('DapiService', () => {
  let service: DapiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DapiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
