import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { concatMap, forkJoin, observable, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
@Injectable()
export class DapiService {

  constructor(
    private http: HttpClient
    ) {}

  getDigimonList(
    name: string,
    level: number,
    description: string,
    atribute: string
  ): Observable<any> {
    const {
      dapi: { base = '' },
    } = environment;
    let httpParams = new HttpParams();
    httpParams = httpParams.append('name', name);
    httpParams = httpParams.append('level', level);
    httpParams = httpParams.append('description', description);
    httpParams = httpParams.append('atribute', atribute);
    return this.http
      .get(`${base}/digimon`, {
        params: httpParams,
      })
      .pipe(
        concatMap((digimons: any) => {
          const obs = digimons.content.map((redir: any) => {
            return this.http.get(redir.href); //https://digimon-api.com/api/v1/digimon/1.split('/api/v1/')[1]
          });
          return forkJoin(obs);
        })
      );
  }
  getDigimonDetail(from: number, size: number): Observable<any> {
    const {
      dapi: { base = '' },
    } = environment;
    let httpParams = new HttpParams();
    httpParams = httpParams.append('from', from);
    httpParams = httpParams.append('size', size);
    return this.http.get(`${base}/digimon/1`);
  }
}
