import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

    { path: 'home',
    loadChildren: () => import('./page/home/home.module').then(m => m.HomeModule) 
  },{ path: 'poc',
    loadChildren: () => import('./page/poc/poc.module').then(m => m.PocModule) 
  }
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
