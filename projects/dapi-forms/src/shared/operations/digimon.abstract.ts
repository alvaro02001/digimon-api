export abstract class  DigimonDataAbstract{
    abstract getDigimon(form: any): any;

    abstract cleanDigimon(): any;
}