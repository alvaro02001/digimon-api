import { NgModule } from '@angular/core';
import { DapiFormsComponent } from './dapi-forms.component';



@NgModule({
  declarations: [
    DapiFormsComponent
  ],
  imports: [
  ],
  exports: [
    DapiFormsComponent
  ]
})
export class DapiFormsModule { }
