import { TestBed } from '@angular/core/testing';

import { DapiFormsService } from './dapi-forms.service';

describe('DapiFormsService', () => {
  let service: DapiFormsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DapiFormsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
