import { formatPercent } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DigimonDataAbstract } from '../../shared/operations/digimon.abstract';

interface Icard{
  name: string,
  body: any,
  actions: any
}

@Component({
  selector: 'app-card',
  templateUrl:'./card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, DigimonDataAbstract{
    title = 'Digimon'
    @Input()cardConfig: Icard = {
      name: '',
      actions:[],
      body:''
    }

    @Output () filterBy: EventEmitter<any> = new EventEmitter();

    public digimonForm:FormGroup
    constructor(
      private fb: FormBuilder
    ){
      this.digimonForm = this.fb.group({
        name: ['', Validators.required,],
        level: ['',Validators.required],
        atribute: ['',Validators.required],
        description: ['',Validators.required]
      })
    }

    ngOnInit(): void {
      this.digimonForm.valueChanges.subscribe(data => {
        console.log(data.name);
      })
    }

    getDigimon(form: FormGroup) {
      // console.log(form.getRawValue());
      // console.log(form.get('name')?.value);
      let result = undefined;
      if(form.get('name')?.value !== ""){
        result = {
          name: form.get('name')?.value
        }
      }

      if(form.get('level')?.value !== ""){
        result = {
          ...result,
          level: form.get('level')?.value
        }
      }

      if(form.get('atribute')?.value !== ""){
        result = {
          ...result,
          atribute: form.get('atribute')?.value
        }
      }

      if(form.get('description')?.value !== ""){
        result = {
          ...result,
          description: form.get('description')?.value
        }
      }

      this.filterBy.emit(form);
    }

    cleanDigimon(){

      this.digimonForm.get('name')?.setValue('');
      this.digimonForm.get('level')?.setValue('');
      this.digimonForm.get('atribute')?.setValue('');
      this.digimonForm.get('description')?.setValue('');
      this.digimonForm.updateValueAndValidity();
    }

}
