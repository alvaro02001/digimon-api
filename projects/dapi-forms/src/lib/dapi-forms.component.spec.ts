import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DapiFormsComponent } from './dapi-forms.component';

describe('DapiFormsComponent', () => {
  let component: DapiFormsComponent;
  let fixture: ComponentFixture<DapiFormsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DapiFormsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DapiFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
