import { NgModule } from '@angular/core';
import { DapiComponent } from './dapi.component';



@NgModule({
  declarations: [
    DapiComponent
  ],
  imports: [
  ],
  exports: [
    DapiComponent
  ]
})
export class DapiModule { }
