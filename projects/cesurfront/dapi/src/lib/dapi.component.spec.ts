import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DapiComponent } from './dapi.component';

describe('DapiComponent', () => {
  let component: DapiComponent;
  let fixture: ComponentFixture<DapiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DapiComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DapiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
