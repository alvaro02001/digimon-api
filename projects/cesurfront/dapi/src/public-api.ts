/*
 * Public API Surface of dapi
 */

export * from './lib/dapi.service';
export * from './lib/dapi.component';
export * from './lib/dapi.module';
